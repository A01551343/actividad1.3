defmodule Tablature do
  def parse(tab) do
    tab
    |> String.split()
    |> Enum.map(fn line -> parse_line(line) end)
    #|> Enum.filter(fn lista -> lista != [] end)
    #|> Enum.zip
    #|> Enum.map(fn t -> Tuple.to_list(t) |> Enum.join(" ") end)
    #|> Enum.join(" ")
  end

  def parse_line(line) do
    #Regex.scan(~r/\d/, line)
    #|> List.flatten
    #|> Enum.map(fn e -> String.at(line,0) <> e end)
    line |> String.graphemes
    |> Enum.with_index
    |> Enum.filter(fn {a,_b} -> a =~ ~r/\d/ end)
    |> Enum.group_by(fn {_a,b} -> b end)
    |> Enum.map(fn {n,p} -> {String.at(line,0) <> n,p} end)

  end
end
